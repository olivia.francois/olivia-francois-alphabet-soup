import main.java.AlphabetSoup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class AlphabetSoupTest {

    AlphabetSoup as;

    @BeforeEach
    void setUp() {
        as = new AlphabetSoup();
    }

    @Test
    void testWordSearch1() {
        File file = new File("src/main/java/resources/input1.txt");
        assertEquals("ABC 0:0 0:2\n" + "AEI 0:0 2:2\n", as.wordSearch(file),
                "Output is correct");
    }

    @Test
    void testWordSearch2() {
        File file = new File("src/main/java/resources/input2.txt");
        assertEquals("HELLO 0:0 4:4\n" + "GOOD 4:0 4:3\n" + "BYE 1:3 1:1\n", as.wordSearch(file),
                "Output is correct");
    }

}