package main.java;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class AlphabetSoup {

    public char[][] grid;
    public int rows = 0;
    public int cols = 0;
    public Map<String, String> wordsToFind = new HashMap<>();

    public AlphabetSoup() {}

    public String wordSearch(File myObj) {
        try {
            Scanner myReader = new Scanner(myObj);
            int index = 1;
            int gridIndex = 0;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if(index == 1) {
                    String[] dimensions = data.split("x");
                    rows = Integer.parseInt(dimensions[0]);
                    cols = Integer.parseInt(dimensions[1]);
                    grid = new char[rows][cols];
                } else if(index > 1 && index <= rows + 1) {
                    String trimData = data.replaceAll(" ", "");
                    for (int j = 0; j < trimData.length(); j++) {
                        grid[gridIndex][j] = trimData.charAt(j);
                    }
                    gridIndex++;
                } else if(index > rows + 1) {
                    wordsToFind.put(data, "");
                }

                index++;
            }

            String stringBuilder = "";
            for(String word: wordsToFind.keySet()) {
                gridSearch(word);
                System.out.println(word + " " + wordsToFind.get(word));
                stringBuilder += word + " " + wordsToFind.get(word) + "\n";
            }

            myReader.close();
            return stringBuilder;
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return "";
    }

    private void gridSearch(String word) {
       for(int row = 0; row < rows; row++) {
           for(int col = 0; col < cols; col++) {
               if(grid[row][col] != word.charAt(0)) {
                   continue;
               } else if(grid[row][col] == word.charAt(0)) {
                   confirmWord(row, col, word);
               }
           }
       }
    }

    private void confirmWord(int row, int col, String word) {
        int[][] oneSpaceDirections = {{-1, -1}, {-1, 0}, {-1, 1}, {1, -1}, {1, 0}, {1, 1}, {0, -1}, {0, 1}};

        int currRow;
        int currCol;
        int matchedChars = 0;

        for (int entry = 0; entry < 8; entry++) {
            currRow = oneSpaceDirections[entry][0] + row;
            currCol =  oneSpaceDirections[entry][1] + col;
            matchedChars = 1;
            for(int j = 1; j < word.length(); j++) {

                if(currRow < 0 || currRow >= rows || currCol < 0 || currCol >= cols) {
                    break;
                } else if(grid[currRow][currCol] != word.charAt(j)) {
                    break;
                } else {
                    if(j < word.length() - 1) {
                        currRow += oneSpaceDirections[entry][0];
                        currCol += oneSpaceDirections[entry][1];
                    }
                    matchedChars++;
                }
            }

            if(matchedChars == word.length()) {
                wordsToFind.put(word, row+":"+ col + " " + currRow+ ":" + currCol);
                break;
            }
        }
    }
}
